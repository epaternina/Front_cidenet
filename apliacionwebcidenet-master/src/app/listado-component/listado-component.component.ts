import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { EmpleadosModel } from '../models/Empleados.Models';
import { ApiResponseModel } from '../models/ApiResponse.Model';
import { environment } from 'src/environments/environment';
import { EmpleadosFilter } from '../models/EmpleadosFilter.Model';
import { FormGroup, FormControl } from '@angular/forms';
import { empleadosService } from '../service/empleados-service.service';
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'app-listado-component',
  templateUrl: './listado-component.component.html',
  styleUrls: ['./listado-component.component.css']
})
export class ListadoComponentComponent  implements OnInit{

  public dataSource: MatTableDataSource<EmpleadosModel> = new MatTableDataSource<EmpleadosModel>([]);
  public selection: Array<EmpleadosModel> = [];
  public AllSelect: boolean = false;
  public length: number =0;
  public pageSizeOptions: Array<number> = environment.pagination.pageSizeOptions;
  public pageSize: number = environment.pagination.pageSizeInit;
  public pageIndex: number = environment.pagination.pageIndexInit;
  public IdentificationTypeDataSource: Array<any> =[];
  public PaisTypeDataSource: Array<any> =[];
  public EmpleadosListaEdit: Array<EmpleadosModel> =[];
  public AreasTypeDataSource: Array<any> =[];
  public Filtrar: boolean = false;
  public fechaActual: string=   new Date().toLocaleString();
  public obligatorio: boolean = true; 
  public FechaDisabled: boolean = true; 
  public FechaIngresoMostrar = new Date().toLocaleString();
  public BotonActualizar: boolean = false
  public objetofecha: Date = new Date();

  minDate = new Date(this.objetofecha.getFullYear(), this.objetofecha.getMonth(), (this.objetofecha.getDate()-30));
  maxDate = new Date(this.objetofecha.getFullYear(), this.objetofecha.getMonth(),this.objetofecha.getDate());



  public reportIssued: FormGroup = new FormGroup({
    PrimerApellido: new FormControl(''),
    SegundoApellido: new FormControl(''),
    PrimerNombre: new FormControl(''),
    OtrosNombres: new FormControl(''),
    TipoIdentificacion: new FormControl(),
    NumeroIdentificacion: new FormControl(''),
    pais: new FormControl(),
    correo: new FormControl(''),
    estado:new FormControl(true),
    pageIndex: new FormControl(),
    pageSize: new FormControl(),
  });

  public Formulario: FormGroup = new FormGroup({
    id_Empleados: new FormControl(),
    nm_PrimerApellido: new FormControl(''),
    nm_SegundoApellido: new FormControl(''),
    nm_PrimerNombre: new FormControl(''),
    nm_OtrosNombres: new FormControl(''),
    Id_Pais:new FormControl(),
    TipoIdentificacion:new FormControl(),
    cd_NumeroIdentificacion:new FormControl(''),
    dt_FechaIngreso:new FormControl(),
    Id_Area:new FormControl(),
    estadoF: new FormControl(true),
  });



  public displayedColumns: Array<string> = ['select', 'Primer Apellido', 'Segundo Apellido', 'Primer Nombre', 'Otros Nombres', 'País',
    'Tipo de Identificación', 'Número de identificación', 'Correo electrónico', 'Fecha de Ingreso',
    'Área', 'Estado', 'Fecha y Hora de Registro'];

    constructor(
      private service: empleadosService,
      private _snackBar: MatSnackBar
    ) { }

    ngOnInit() {
      this.submitFilters();
      this.cargarinfo();
     
    }


    cargarinfo(){
      this.service.getIdentificationTypes().subscribe(
        (IdentificationTypes:any)=>{
          this.IdentificationTypeDataSource =IdentificationTypes;
      }
      );

      this.service.getPaises().subscribe(
        (paises:any)=>{
          this.PaisTypeDataSource =paises;
      }
      );

      this.service.getAreas().subscribe(
        (areas:any)=>{
          this.AreasTypeDataSource =areas;
      }
      );


    }

     /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle(event: any) {

    this.dataSource.data.forEach(element => {
      element.check = !event;
    });
    var data = this.dataSource.data;
    this.selection = !event ? data : [];
    this.AllSelect = !event;
  }

  isSelect(index: EmpleadosModel) {

    this.dataSource.data.forEach(element => {
      if (index == element) {
        if (index.check) {
          element.check = false;

          for (let index1 = 0; index1 < this.selection.length; index1++) {

            const element1 = this.selection[index1];
            if (element1 === undefined) { continue; }
            if (element1.id_Empleados == index.id_Empleados) {
              this.selection = this.selection.filter(r => r.id_Empleados !== index.id_Empleados);
            }

          }

        } else {
          element.check = true;
        }
      }

    });

    for (let index = 0; index < this.dataSource.data.filter(r => r.check == true).length; index++) {
      const element = this.dataSource.data.filter(r => r.check == true)[index];
      if (this.selection.length == 0) {
        this.selection.push(element);
      } else if (this.selection.filter(r => r.id_Empleados == element.id_Empleados).length == 0) {
        this.selection.push(element);
      }
    }
    this.AllSelect = this.selection.length == this.dataSource.data.length ? true : false;
  }

  getDataPaginated(event: any) {
    const pagination = {
      pageIndex: event.pageIndex + 1,
      pageSize: event.pageSize
    }
    this.pageIndex = pagination.pageIndex;
    this.pageSize = pagination.pageSize;
    this.submitFilters(pagination);
  }

  buildFilters(pagination?: any): EmpleadosFilter {
    let dataRequest: EmpleadosFilter = this.reportIssued.value;

    dataRequest.pageIndex = pagination ? pagination.pageIndex : this.pageIndex;
    dataRequest.pageSize = pagination ? pagination.pageSize : this.pageSize;

    return dataRequest;
  }

  submitFilters(pagination?: any) {
    let dataRequest = this.buildFilters(pagination);


    this.service.getAllEmpleados(dataRequest).subscribe(
      (response: ApiResponseModel<EmpleadosModel>) => {
        this.length = response.totalItemCount;
        this.dataSource = new MatTableDataSource<EmpleadosModel>(response.items);
      },
      (error: any) => {
        console.log('Error al procesar' + error)
      }
    )
  }

  buildForm() {
    this.reportIssued.reset();
    this.submitFilters();
  }
  submitFormulario(){
    if(this.Formulario.invalid){
      this.openSnackBar('Los campos marcados en rojo deben ser verificados', 'Aceptar');
      return;
  }

    var obj=
      {
      "nm_PrimerApellido":this.Formulario.controls['nm_PrimerApellido'].value,
      "nm_SegundoApellido": this.Formulario.controls['nm_SegundoApellido'].value,
      "nm_PrimerNombre":this.Formulario.controls['nm_PrimerNombre'].value,
      "nm_OtrosNombres":this.Formulario.controls['nm_OtrosNombres'].value,
      "Id_Pais": parseInt(this.Formulario.controls['Id_Pais'].value),
      "TipoIdentificacion":parseInt(this.Formulario.controls['TipoIdentificacion'].value),
      "cd_NumeroIdentificacion": this.Formulario.controls['cd_NumeroIdentificacion'].value,
      "dt_FechaIngreso":this.Formulario.controls['dt_FechaIngreso'].value,
      "Id_Area":parseInt(this.Formulario.controls['Id_Area'].value)
      }
    this.service.postEmpleados(obj).subscribe(
      (Respuesta:any)=>{
        this.openSnackBar(Respuesta, 'Aceptar') 
        this.submitFilters();
        this.buildFormRegistro();
        this.dismiss();
    },
    (error) => {;
      this.openSnackBar(error.message, 'Aceptar');
    }
    );
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }

  dismiss(){
    setTimeout(() => {
      this._snackBar.dismiss();
     },6000);
  }

  buildFormRegistro(){
    this.Formulario.reset();
    this.BotonActualizar = false;
    this.FechaDisabled = true;
  }

  FiltrarFunction(){
    if(this.Filtrar)
        this.Filtrar = false;
    else
      this.Filtrar= true;
  }
  

  // Only AlphaNumeric
keyPressAlphaNumeric(event:any) {

  var inp = String.fromCharCode(event.keyCode);

  if (/[a-zA-Z]/.test(inp)) {
    return true;
  } else {
    event.preventDefault();
    return false;
  }
}


EditarEmpleado(){
if(this.selection.length>0 && this.selection.length<2){

  this.service.getOneEmpleados(this.selection[0].cd_NumeroIdentificacion).subscribe(
    (empleado:any)=>{
      this.EmpleadosListaEdit =empleado;
      this.Formulario.controls['id_Empleados'].setValue(this.EmpleadosListaEdit[0].id_Empleados);
      this.Formulario.controls['nm_PrimerApellido'].setValue(this.EmpleadosListaEdit[0].nm_PrimerApellido);
      this.Formulario.controls['nm_SegundoApellido'].setValue(this.EmpleadosListaEdit[0].nm_SegundoApellido);
      this.Formulario.controls['nm_PrimerNombre'].setValue(this.EmpleadosListaEdit[0].nm_PrimerNombre);
      this.Formulario.controls['nm_OtrosNombres'].setValue(this.EmpleadosListaEdit[0].nm_OtrosNombres);
      this.Formulario.controls['Id_Pais'].setValue(this.EmpleadosListaEdit[0].Id_Pais);
      this.Formulario.controls['TipoIdentificacion'].setValue(this.EmpleadosListaEdit[0].TipoIdentificacion);
      this.Formulario.controls['cd_NumeroIdentificacion'].setValue(this.EmpleadosListaEdit[0].cd_NumeroIdentificacion);
      this.Formulario.controls['dt_FechaIngreso'].setValue(this.EmpleadosListaEdit[0].dt_FechaIngreso);
      this.Formulario.controls['Id_Area'].setValue(this.EmpleadosListaEdit[0].Id_Area);
      this.selection=[];
      this.AllSelect=false;
      this.BotonActualizar = true;
      this.FechaDisabled = false;
      this.FechaIngresoMostrar = this.EmpleadosListaEdit[0].dt_FechaIngreso.toLocaleString();

  }
  );
  }
}
actualizarRegistro(){
  if(this.Formulario.invalid){
    this.openSnackBar('Los campos marcados en rojo deben ser verificados', 'Aceptar');
    return;
}

var obj=
  {
  "nm_PrimerApellido":this.Formulario.controls['nm_PrimerApellido'].value,
  "nm_SegundoApellido": this.Formulario.controls['nm_SegundoApellido'].value,
  "nm_PrimerNombre":this.Formulario.controls['nm_PrimerNombre'].value,
  "nm_OtrosNombres":this.Formulario.controls['nm_OtrosNombres'].value,
  "Id_Pais": parseInt(this.Formulario.controls['Id_Pais'].value),
  "TipoIdentificacion":parseInt(this.Formulario.controls['TipoIdentificacion'].value),
  "cd_NumeroIdentificacion": this.Formulario.controls['cd_NumeroIdentificacion'].value,
  "dt_FechaIngreso":this.Formulario.controls['dt_FechaIngreso'].value,
  "Id_Area":parseInt(this.Formulario.controls['Id_Area'].value)
}

this.service.putEmpleados(obj, this.Formulario.controls['id_Empleados'].value).subscribe(
  (Respuesta:any)=>{
    this.openSnackBar(Respuesta, 'Aceptar') 
    this.submitFilters();
    this.buildFormRegistro();
    this.dismiss();
    this.selection=[];
    this.AllSelect=false;
  },
  (error) => {;
  this.openSnackBar(error.message, 'Aceptar');
  this.dismiss();
  this.selection=[];
  this.AllSelect=false;
  });
    
}

EliminarEmpleado(){
  if(this.selection.length>0 && this.selection.length<2){

    if (window.confirm("Está seguro de que desea eliminar el empleado? Aceptar / Cancelar")) {
      this.service.deleteEmpleados(this.selection[0].cd_NumeroIdentificacion).subscribe(
        (Respuesta:any)=>{
          this.openSnackBar(Respuesta, 'Aceptar') 
          this.submitFilters();
          this.buildFormRegistro();
          this.dismiss();
          this.selection=[];
          this.AllSelect=false;
        },
        (error) => {;
        this.openSnackBar(error.message, 'Aceptar');
        this.dismiss();
        this.selection=[];
        this.AllSelect=false;
        });
    }

  
  }
 
}

}
