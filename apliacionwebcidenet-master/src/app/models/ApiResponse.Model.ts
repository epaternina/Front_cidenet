export interface ApiResponseModel<T> {
    additionalItems: any
    items: Array<T>;
    pageNumber: number;
    pageSize:number;
    totalItemCount: number;
    totalPages: number;
  }