export interface EmpleadosModel{
    id_Empleados: number ;
    nm_PrimerApellido: string;
    nm_SegundoApellido: string;
    nm_PrimerNombre :String;
    nm_OtrosNombres: string;
    nm_Pais: string;
    Id_Pais: number ;
    cd_TipoIdentificacion: string;
    TipoIdentificacion: number ;
    cd_NumeroIdentificacion: string;
    nm_CorreoElectronico: string;
    dt_FechaIngreso:Date;
    nm_Area: string;
    Id_Area: number ;
    ind_Estado: boolean;
    dt_FechaHoraRegistro:Date;
    dt_Creado:Date;
    check?:boolean;
}