export interface GeneralTypeModel {
    value: string;
    description: string;
}