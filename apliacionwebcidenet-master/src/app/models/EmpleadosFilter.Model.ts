export interface EmpleadosFilter{
    PrimerApellido:string;
    SegundoApellido: string;
    PrimerNombre: string;
    OtrosNombres: string;
    TipoIdentificacion: number;
    NumeroIdentificacion: string;
    pais: number;
    correo: string;
    estado: boolean;
    pageIndex:number;
    pageSize:number;
}