import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { EmpleadosFilter } from '../models/EmpleadosFilter.Model';
import { ApiResponseModel } from '../models/ApiResponse.Model';
import { EmpleadosModel } from '../models/Empleados.Models';
import { GeneralTypeModel } from '../models/GeneralTypeModel';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class empleadosService {
    private apiUrl: string;
    private headers: HttpHeaders;
    constructor(private http: HttpClient) {
        this.apiUrl = environment.apiUrl;
        this.headers = new HttpHeaders() 
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json');
    }

    
MapParametersHttp(filters: any) {
    let parameters = new HttpParams();

    Object.keys(filters).forEach(key => {
      if (filters[key]) {
        parameters = parameters.append(key, filters[key]);
      }
    });

    return parameters;
  }

  getAllEmpleados(filter: EmpleadosFilter): Observable<ApiResponseModel<EmpleadosModel>> {
    const params = this.MapParametersHttp(filter);
    const url = `${this.apiUrl}api/Empleados/All`;
    return this.http.get<any>(url,
      {
        params,
        headers: this.headers
      }
    );
  }

  getOneEmpleados(cd_NumeroIdentificacion:string): Observable<EmpleadosModel[]> {
    var url = `${this.apiUrl}api/Empleados/GetOne?cd_NumeroIdentificacion=${cd_NumeroIdentificacion}`;
    return this.http.get<any>(url, { headers: this.headers});
  }


  getIdentificationTypes(): Observable<GeneralTypeModel[]> {
    return this.http.get<GeneralTypeModel[]>(`${this.apiUrl}api/Empleados/TipoIdentificacion`,
      {
        headers: this.headers
      });
  }

  getPaises(): Observable<GeneralTypeModel[]> {
    return this.http.get<GeneralTypeModel[]>(`${this.apiUrl}api/Empleados/Paises`,
      {
        headers: this.headers
      });
  }

  getAreas(): Observable<GeneralTypeModel[]> {
    return this.http.get<GeneralTypeModel[]>(`${this.apiUrl}api/Empleados/Areas`,
      {
        headers: this.headers
      });
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      "Access-Control-Allow-Origin": "*",
      
    } ),responseType: 'text' as 'json'
  };

  postEmpleados(parametros: object ): Observable<any> {
    const body = parametros;
    const url = `${this.apiUrl}api/Empleados`;
    return this.http.post<any>(url,body,this.httpOptions);
  }

  putEmpleados(parametros: object, id:number ): Observable<any> {
    const body = parametros;
    const url = `${this.apiUrl}api/Empleados/${id}`;
    return this.http.put<any>(url,body,this.httpOptions);
  }

 
  deleteEmpleados(id:string): Observable<any> {
    const body = {};
    const url = `${this.apiUrl}api/Empleados/${id}`;
    return this.http.delete<any>(url,this.httpOptions);
  }

}